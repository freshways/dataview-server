package com.base.mapper;

import com.base.vo.WorkBook;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @Entity com.base.vo.WorkBook
 */
@Repository
public interface WorkBookMapper extends BaseMapper<WorkBook> {

}




