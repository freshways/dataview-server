package com.base.mapper;

import com.base.vo.RepositorySource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.base.vo.RepositorySource
 */
public interface RepositorySourceMapper extends BaseMapper<RepositorySource> {

}




