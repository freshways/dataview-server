package com.base.mapper;

import com.base.vo.Dashboard;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.base.vo.Dashboard
 */
public interface DashboardMapper extends BaseMapper<Dashboard> {

}




