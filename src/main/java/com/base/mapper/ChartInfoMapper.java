package com.base.mapper;

import com.base.vo.ChartInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.base.vo.ChartInfo
 */
public interface ChartInfoMapper extends BaseMapper<ChartInfo> {

}




