package com.base.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.vo.DataSource;

import java.util.LinkedHashMap;
import java.util.List;

/**
* @Entity com.base.vo.DataSource
*/
public interface DataSourceMapper extends BaseMapper<DataSource> {

    Page selectBySql(Page page,String sqlText, Wrapper ew);

    /**
     *
     * @param page
     * @param yoyType 同环比周期
     * @param sqlText
     * @param ew
     * @return
     */
    Page selectYoYBySql(Page page,String yoyType,String sqlText, Wrapper ew);

    Page selectByDataSourceWithPatientId(Page page,String sqlText, Wrapper ew);
}
