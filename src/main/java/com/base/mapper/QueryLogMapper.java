package com.base.mapper;

import com.base.vo.QueryLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.base.vo.QueryLog
 */
public interface QueryLogMapper extends BaseMapper<QueryLog> {

}




