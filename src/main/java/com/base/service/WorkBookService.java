package com.base.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.base.vo.PageRequestHelperVo;
import com.base.vo.WorkBook;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface WorkBookService extends IService<WorkBook> {
    /**
     * 分页请求
     * @param pageRequestHelperVo
     * @return
     */
    IPage page(PageRequestHelperVo pageRequestHelperVo);
}
