package com.base.service;

import com.base.vo.ChartInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 */
public interface ChartInfoService extends IService<ChartInfo> {

}
