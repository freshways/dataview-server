package com.base.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.base.vo.PageRequestHelperVo;
import com.base.vo.RepositorySource;
import com.baomidou.mybatisplus.extension.service.IService;

import java.sql.SQLException;

/**
 *
 */
public interface RepositorySourceService extends IService<RepositorySource> {

    /**
     * 校验连接
     * @param repositorySource
     * @return
     */
    boolean testConnection(RepositorySource repositorySource);

    IPage page(PageRequestHelperVo pageRequestHelperVo);
}
