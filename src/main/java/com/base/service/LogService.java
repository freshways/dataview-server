package com.base.service;

import java.io.IOException;
import java.util.List;

/**
 * @author wnhuang
 * @Package com.base.service
 * @date 2021/8/19 15:32
 */
public interface LogService {
    /**
     * 获取日志目录列表
     * @param path 路径
     * @return
     */
    List<String> getLogFileNames(String path);

    /**
     * 获取日志文件内容
     * @param fileName
     * @return
     */
    String getLogFileByName(String fileName) throws IOException;
}
