package com.base.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.dialects.Dialects;
import com.base.query.CustomizeQueryWrapper;
import com.base.vo.Field;
import com.base.vo.Filters;

import java.util.List;

/**
 * SQL语句构建器
 * @author wnhuang
 * @Package com.base.api.service
 * @date 2021-03-18 23:50
 */
public interface SqlStatementBuilder {

    /**
     * 构建SELECT部分
     * @param fieldsList
     * @param queryWrapper
     * @param dialect
     * @return
     */
    void builderSelect(List<Field> fieldsList, CustomizeQueryWrapper queryWrapper, Dialects dialect);

    /**
     * 构建WHERE部分
     * @param filtersList
     * @param dialect
     * @return
     */
    void builderWhere(List<Filters> filtersList, CustomizeQueryWrapper queryWrapper, Dialects dialect);

    /**
     * 构建GROUPBY部分
     * @param fieldsList
     * @param dialect
     * @return
     */
    void builderGroup(List<Field> fieldsList, CustomizeQueryWrapper queryWrapper);

    /**
     * 构建ORDERBY部分
     * @param fieldsList
     * @return
     */
    void builderOrderBy(List<Field> fieldsList, CustomizeQueryWrapper queryWrapper);
}
