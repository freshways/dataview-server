package com.base.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.base.vo.Dashboard;
import com.baomidou.mybatisplus.extension.service.IService;
import com.base.vo.DataSource;
import com.base.vo.PageRequestHelperVo;

/**
 *
 */
public interface DashboardService extends IService<Dashboard> {

    /**
     * 分页查询
     * @param pageRequestHelperVo
     * @return
     */
    IPage<DataSource> page(PageRequestHelperVo pageRequestHelperVo);
}
