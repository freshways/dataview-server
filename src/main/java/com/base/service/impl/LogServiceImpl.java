package com.base.service.impl;

import com.base.service.LogService;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author wnhuang
 * @Package com.base.service.impl
 * @date 2021/8/19 15:34
 */
@Service
public class LogServiceImpl implements LogService {

    /**
     * 获取日志目录列表
     *
     * @return
     */
    @Override
    public List<String> getLogFileNames(String path) {
        File file = new File(path);
        Collection<File> files1 = FileUtils.listFiles(file, new String[]{"log"}, false);
        return files1.stream().map(File::getName).collect(Collectors.toList());
    }

    /**
     * 获取日志文件内容
     *
     * @param fileName
     * @return
     */
    @Override
    public String getLogFileByName(String fileName) throws IOException {
        return FileUtils.readFileToString(new File(fileName), "UTF-8");
    }
}
