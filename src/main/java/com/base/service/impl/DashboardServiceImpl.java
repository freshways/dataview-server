package com.base.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.base.vo.Dashboard;
import com.base.service.DashboardService;
import com.base.mapper.DashboardMapper;
import com.base.vo.DataSource;
import com.base.vo.PageRequestHelperVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class DashboardServiceImpl extends ServiceImpl<DashboardMapper, Dashboard>
        implements DashboardService {

    /**
     * 分页查询
     *
     * @param pageRequestHelperVo
     * @return
     */
    @Override
    public IPage<DataSource> page(PageRequestHelperVo pageRequestHelperVo) {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.like(StringUtils.isNotBlank(pageRequestHelperVo.getKeyword()), "name", pageRequestHelperVo.getKeyword());
        IPage<Dashboard> page = new Page<>(pageRequestHelperVo.getPageNum(), pageRequestHelperVo.getPageSize());
        return page(page, wrapper);
    }
}




