package com.base.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.base.mapper.QueryLogMapper;
import com.base.service.QueryLogService;
import com.base.vo.QueryLog;
import org.apache.commons.lang.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 *
 */
@Service
public class QueryLogServiceImpl extends ServiceImpl<QueryLogMapper, QueryLog>
        implements QueryLogService {

    /**
     * 异步保存查询日志到数据库
     *
     * @param entity 日志对象
     */
    @Override
    @Async
    public void asyncSaveOrUpdate(QueryLog entity) {
        super.saveOrUpdate(entity);
    }

    /**
     * 分页条件查询
     *
     * @param requestMap 请求内容
     * @return 查询结果
     */
    @Override
    public Page<QueryLog> page(Map<String, Object> requestMap) {
        Page<QueryLog> page = new Page<>((int) requestMap.get("pageNum"), (int) requestMap.get("pageSize"));
        QueryWrapper<QueryLog> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank((String) requestMap.get("type")), "exec_result", requestMap.get("type"));
        return page(page, wrapper);
    }
}




