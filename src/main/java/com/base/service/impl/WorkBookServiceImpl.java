package com.base.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.base.vo.PageRequestHelperVo;
import com.base.vo.WorkBook;
import com.base.service.WorkBookService;
import com.base.mapper.WorkBookMapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class WorkBookServiceImpl extends ServiceImpl<WorkBookMapper, WorkBook>
        implements WorkBookService {

    /**
     * 分页请求
     *
     * @param pageRequestHelperVo
     * @return
     */
    @Override
    public IPage<WorkBook> page(PageRequestHelperVo pageRequestHelperVo) {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.like(StringUtils.isNotBlank(pageRequestHelperVo.getKeyword()),"name",pageRequestHelperVo.getKeyword());
        wrapper.eq(StringUtils.isNotBlank(pageRequestHelperVo.getType()),"chart_mode",pageRequestHelperVo.getType());
        IPage<WorkBook> page = new Page<>(pageRequestHelperVo.getPageNum(), pageRequestHelperVo.getPageSize());
        return page(page,wrapper);
    }
}




