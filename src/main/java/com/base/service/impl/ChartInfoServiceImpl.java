package com.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.base.vo.ChartInfo;
import com.base.service.ChartInfoService;
import com.base.mapper.ChartInfoMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class ChartInfoServiceImpl extends ServiceImpl<ChartInfoMapper, ChartInfo>
implements ChartInfoService{

}




