package com.base.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.base.vo.DataSource;
import com.base.vo.ExecQueryVo;
import com.base.vo.PageRequestHelperVo;


import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 */
public interface DataSourceService extends IService<DataSource> {

    /**
     * 分页查询
     *
     * @param pageRequestHelperVo
     * @return
     */
    IPage<DataSource> page(PageRequestHelperVo pageRequestHelperVo);

    /**
     * 执行查询，返回查询结果
     *
     * @param execQueryVo
     * @return
     */
    Page execQuery(ExecQueryVo execQueryVo);


}
