package com.base.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.base.vo.QueryLog;

import java.util.Map;

/**
 *
 */
public interface QueryLogService extends IService<QueryLog> {

    /**
     * 异步保存查询日志到数据库
     * @param entity 查询日志对象
     */
    void asyncSaveOrUpdate(QueryLog entity);

    /**
     * 分页条件查询
     * @param requestMap 请求参数对象
     * @return
     */
    Page<QueryLog> page(Map<String,Object> requestMap);
}
