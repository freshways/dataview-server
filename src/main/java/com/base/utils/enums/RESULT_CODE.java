package com.base.utils.enums;

/**
 * 响应码
 *
 * @author wnhuang
 * @date 2020/1/6
 */
public enum RESULT_CODE {
    SUCCESS(0, "成功"),
    WAIT(200, "请稍后"),
    FAIL(400, "失败");

    private Integer code;

    private String maessage;

    RESULT_CODE(Integer code, String maessage) {
        this.code = code;
        this.maessage = maessage;
    }

    public Integer getCode() {
        return code;
    }

    public String getMaessage() {
        return maessage;
    }
}
