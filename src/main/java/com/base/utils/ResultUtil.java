package com.base.utils;

import com.base.utils.enums.RESULT_CODE;

/**
 * 响应结果工具类.
 *
 * @author wnhuang
 * @date 2020/1/6
 */
public class ResultUtil {

    /**
     * 生成成功结果并绑定数据.
     *
     * @param data 绑定数据
     * @return json
     */
    private static <T> Result success(String msg, T data) {
        return new Result(RESULT_CODE.SUCCESS.getCode(),msg,data);
    }

    /**
     * 生成成功结果.
     *
     * @return json
     */
    public static <T> Result success(T data) {
        return success("", data);
    }


    /**
     * 生成成功结果.
     *
     * @return json
     */
    public static <T> Result success(String msg) {
        return success(msg, null);
    }

    /**
     * 生成成功结果.
     *
     * @return json
     */
    public static <T> Result success() {
        return success(RESULT_CODE.SUCCESS.getMaessage());
    }

    /**
     * 生成失败结果并绑定数据.
     *
     * @param code 错误码
     * @param msg  错误信息
     * @param data 绑定数据
     * @return json
     */
    private static <T> Result error(Integer code, String msg, T data) {
        return new Result(code,msg,data);
    }

    /**
     * 生成失败结果.
     *
     * @param code 错误码
     * @param msg  错误信息
     * @return json
     */
    public static <T> Result error(Integer code, String msg) {
        return error(code, msg, null);
    }

    public static <T> Result error(String msg) {
        return error(RESULT_CODE.FAIL.getCode(), msg, null);
    }

}
