package com.base.utils;

import com.base.vo.QueryLog;

/**
 * 存储查询日志工具类
 * @author wnhuang
 * @Package com.base.utils
 * @date 2021/8/20 9:48
 */
public class QueryLogTheadLocalUtils {
    private static final ThreadLocal<QueryLog> threadLocal = new ThreadLocal<>();

    public static void set(QueryLog queryLog) {
        threadLocal.set(queryLog);
    }

    public static QueryLog get() {
        return threadLocal.get();
    }
}
