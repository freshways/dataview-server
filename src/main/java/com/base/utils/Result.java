package com.base.utils;

import java.io.Serializable;

import lombok.Data;


/**
 * 响应结果对象
 *
 * @author wnhuang
 * @Package com.base.utils
 * @date 2021-03-11 21:42
 */
@Data
public class Result<T> implements Serializable {

    public Result() {
        super();
    }

    public Result(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }


    /**
     * 错误码
     */
    private Integer code;

    /**
     * 提示信息
     */
    private String msg;

    /**
     * 具体的内容
     */
    private T data;
}
