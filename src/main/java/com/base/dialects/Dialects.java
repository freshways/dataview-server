package com.base.dialects;

import com.base.vo.Field;

import java.util.List;

/**
 * 数据库方言接口
 *
 * @author wnhuang
 * @Package com.base.dialects
 * @date 2021/7/6 2:22
 */
public interface Dialects {


    /**
     * 构建查询同环比的SQL
     *
     * @param sql
     * @return
     */
    default String buildYoYSql(String sql, List<Field> list) {
        return YoYSqlBuildUtil.buildYoYSql(this, sql, list);
    }

    /**
     * 不同数据库，Date转String的写法
     *
     * @param columnName 字段名
     * @param format     格式
     * @return 对应数据库的写法
     */
    default String convertDateToString(String columnName, String format) {
        return "to_char(" + columnName + ",'" + format + "')";
    }

    /**
     * 计算日期加减
     *
     * @param columnName 日期字段
     * @param isAdd      加还是减
     * @param quantity   数量
     * @param format     表达式(年月日时分秒)
     * @return
     */
    default String computedDateString(String columnName, boolean isAdd, int quantity, String format) {
        String sql = columnName;
        if (isAdd) {
            sql += " + ";
        } else {
            sql += " - ";
        }
        sql += "interval '" + quantity;
        String expr = "";
        switch (format) {
            case "year":
                expr = "year'";
                break;
            case "month":
                expr = "mon'";
                break;
            case "day":
                expr = "day'";
                break;
            case "hour":
                expr = "hour'";
                break;
        }
        sql += expr;
        return sql;
    }

    default String convertStringToDate(String columnName, String format){
        return "to_date(" + columnName + ",'" + format + "')";
    };
}
