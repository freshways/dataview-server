package com.base.dialects;

import java.time.LocalDate;

/**
 * @author wnhuang
 * @Package com.base.dialects
 * @date 2021/7/9 10:46
 */
public class MysqlDialect implements Dialects {
    /**
     * 不同数据库，Date转String的写法
     *
     * @param columnName 字段名
     * @param format     格式
     * @return 对应数据库的写法
     */
    @Override
    public String convertDateToString(String columnName, String format) {
        String newFormat;
        switch (format) {
            case "yyyyMM":
                newFormat = "%Y%m";
                break;
            case "yyyy":
                newFormat = "%Y";
                break;
            default:
                newFormat = "%Y%m%d";
        }
        return "DATE_FORMAT(" + columnName + ",'" + newFormat + "')";
    }
}
