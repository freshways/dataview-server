package com.base.dialects;

import com.baomidou.mybatisplus.annotation.DbType;

import java.util.EnumMap;
import java.util.Map;

/**
 * @author wnhuang
 * @Package com.base.dialects
 * @date 2021/7/9 11:46
 */
public class DialectRegistry {
    private final Map<DbType, Dialects> dialect_enum_map = new EnumMap<>(DbType.class);

    public DialectRegistry() {
        // mysql and children
        dialect_enum_map.put(DbType.MYSQL, new MysqlDialect());

        // postgresql and children
        dialect_enum_map.put(DbType.POSTGRE_SQL, new OracleDialect());

        // oracle and children
        dialect_enum_map.put(DbType.ORACLE, new OracleDialect());

        // other
        dialect_enum_map.put(DbType.ORACLE_12C, new OracleDialect());
        dialect_enum_map.put(DbType.SQL_SERVER, new OracleDialect());

    }

    public Dialects getDialect(DbType dbType) {
        return dialect_enum_map.get(dbType);
    }
}
