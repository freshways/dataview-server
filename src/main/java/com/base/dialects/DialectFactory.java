package com.base.dialects;


import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.toolkit.ExceptionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.dialects.IDialect;

import java.util.Optional;

/**
 * @author wnhuang
 * @Package com.base.dialects
 * @date 2021/7/9 11:56
 */
public class DialectFactory {
    private static final DialectRegistry DIALECT_REGISTRY = new DialectRegistry();

    public static Dialects getDialect(DbType dbType) {
        return Optional.ofNullable(DIALECT_REGISTRY.getDialect(dbType))
                .orElseThrow(() -> ExceptionUtils.mpe("%s database not supported.", dbType.getDb()));
    }
}
