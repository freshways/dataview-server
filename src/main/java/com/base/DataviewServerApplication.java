package com.base;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import springfox.documentation.oas.annotations.EnableOpenApi;

@EnableOpenApi
@EnableAsync
@SpringBootApplication
@MapperScan(value = {"com.base.mapper","com.base.dao"})
public class DataviewServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataviewServerApplication.class, args);
    }

}
