package com.base.vo;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 *
 * @TableName query_log
 */
@TableName(value ="query_log")
@Data
public class QueryLog implements Serializable {
    /**
     * 主键
     */
    @TableId
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @OrderBy
    private Date createTime;

    /**
     * 最后更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastUpdateTime;

    /**
     * 删除标识
     */
    private Byte isDelete;

    /**
     * 分析ID
     */
    private Long dashboardId;

    /**
     * 图表ID
     */
    private Long workBookId;

    /**
     * 数据源ID
     */
    private Long dataSourceId;

    /**
     * 数据库ID
     */
    private Long repositorySourceId;

    /**
     * 执行SQL
     */
    private String execSql;

    /**
     * 查看参数
     */
    private String queryParam;

    /**
     * 日期范围
     */
    private String dateRanges;

    /**
     * 执行耗时,ms
     */
    private Long execTimes;

    /**
     * 批次
     */
    private String batch;

    /**
     * 执行结果,1-成功，0-失败
     */
    private Byte execResult;

    /**
     * 失败信息
     */
    private String execMsg;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        QueryLog other = (QueryLog) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getLastUpdateTime() == null ? other.getLastUpdateTime() == null : this.getLastUpdateTime().equals(other.getLastUpdateTime()))
            && (this.getIsDelete() == null ? other.getIsDelete() == null : this.getIsDelete().equals(other.getIsDelete()))
            && (this.getDashboardId() == null ? other.getDashboardId() == null : this.getDashboardId().equals(other.getDashboardId()))
            && (this.getWorkBookId() == null ? other.getWorkBookId() == null : this.getWorkBookId().equals(other.getWorkBookId()))
            && (this.getDataSourceId() == null ? other.getDataSourceId() == null : this.getDataSourceId().equals(other.getDataSourceId()))
            && (this.getRepositorySourceId() == null ? other.getRepositorySourceId() == null : this.getRepositorySourceId().equals(other.getRepositorySourceId()))
            && (this.getExecSql() == null ? other.getExecSql() == null : this.getExecSql().equals(other.getExecSql()))
            && (this.getQueryParam() == null ? other.getQueryParam() == null : this.getQueryParam().equals(other.getQueryParam()))
            && (this.getDateRanges() == null ? other.getDateRanges() == null : this.getDateRanges().equals(other.getDateRanges()))
            && (this.getExecTimes() == null ? other.getExecTimes() == null : this.getExecTimes().equals(other.getExecTimes()))
            && (this.getBatch() == null ? other.getBatch() == null : this.getBatch().equals(other.getBatch()))
            && (this.getExecResult() == null ? other.getExecResult() == null : this.getExecResult().equals(other.getExecResult()))
            && (this.getExecMsg() == null ? other.getExecMsg() == null : this.getExecMsg().equals(other.getExecMsg()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getLastUpdateTime() == null) ? 0 : getLastUpdateTime().hashCode());
        result = prime * result + ((getIsDelete() == null) ? 0 : getIsDelete().hashCode());
        result = prime * result + ((getDashboardId() == null) ? 0 : getDashboardId().hashCode());
        result = prime * result + ((getWorkBookId() == null) ? 0 : getWorkBookId().hashCode());
        result = prime * result + ((getDataSourceId() == null) ? 0 : getDataSourceId().hashCode());
        result = prime * result + ((getRepositorySourceId() == null) ? 0 : getRepositorySourceId().hashCode());
        result = prime * result + ((getExecSql() == null) ? 0 : getExecSql().hashCode());
        result = prime * result + ((getQueryParam() == null) ? 0 : getQueryParam().hashCode());
        result = prime * result + ((getDateRanges() == null) ? 0 : getDateRanges().hashCode());
        result = prime * result + ((getExecTimes() == null) ? 0 : getExecTimes().hashCode());
        result = prime * result + ((getBatch() == null) ? 0 : getBatch().hashCode());
        result = prime * result + ((getExecResult() == null) ? 0 : getExecResult().hashCode());
        result = prime * result + ((getExecMsg() == null) ? 0 : getExecMsg().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", createTime=").append(createTime);
        sb.append(", lastUpdateTime=").append(lastUpdateTime);
        sb.append(", isDelete=").append(isDelete);
        sb.append(", dashboardId=").append(dashboardId);
        sb.append(", workBookId=").append(workBookId);
        sb.append(", dataSourceId=").append(dataSourceId);
        sb.append(", repositorySourceId=").append(repositorySourceId);
        sb.append(", execSql=").append(execSql);
        sb.append(", queryParam=").append(queryParam);
        sb.append(", dateRanges=").append(dateRanges);
        sb.append(", execTimes=").append(execTimes);
        sb.append(", batch=").append(batch);
        sb.append(", execResult=").append(execResult);
        sb.append(", execMsg=").append(execMsg);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
