package com.base.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 过滤条件
 * @author wnhuang
 * @Package com.base.vo
 * @date 2021-03-18 23:22
 */
@Data
public class Filters implements Serializable {

    //要过滤的字段
    private Field field;

    //过滤模式
    private Integer filterMode;

    //表达式
    private String matchValue;

    //开始日期
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private Date beginDate;

    //结束日期
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private Date endDate;

    //过滤项
    private List<String> filterItems;

    //标记
    private String mark;

    //最小值
    private Integer minValue;

    //最大值
    private Integer maxValue;
}
