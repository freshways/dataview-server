package com.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.Data;

/**
 * repository_config
 * @author 
 */
@ApiModel(value="com.base.vo.RepositoryConfig")
@Data
public class RepositoryConfig implements Serializable {
    /**
     * 数据库连接类型
     */
    @ApiModelProperty(value="数据库连接类型")
    private Integer providerType;

    /**
     * 名称
     */
    @ApiModelProperty(value="名称")
    private String providerName;

    /**
     * 驱动
     */
    @ApiModelProperty(value="驱动")
    private String databaseDriver;

    private static final long serialVersionUID = 1L;
}