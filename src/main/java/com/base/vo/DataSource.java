package com.base.vo;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 数据源
 * @TableName data_source
 */
@TableName(value ="data_source")
@Data
public class DataSource implements Serializable {
    /**
     *
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId
    private Long id;

    /**
     * 名称
     */
    private String name;

    /**
     * 数据仓库ID
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long repositoryId;

    /**
     *
     */
    private Integer dataProviderType;

    /**
     *
     */
    private String preDataFields;

    /**
     *
     */
    private String chooseTables;

    /**
     *
     */
    private String tableRelations;

    /**
     * 查询模式
     */
    private Integer queryType;

    /**
     * 查询语句
     */
    private String syntax;

    /**
     * 前置执行
     */
    private String beforeExecute;

    /**
     *
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     *
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @OrderBy
    private Date lastUpdateTime;

    /**
     *
     */
    private String lastUpdateUserId;

    /**
     *
     */
    private String createUserId;

    /**
     *
     */
    private String description;

    /**
     *
     */
    private Byte isDelete;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DataSource other = (DataSource) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getRepositoryId() == null ? other.getRepositoryId() == null : this.getRepositoryId().equals(other.getRepositoryId()))
            && (this.getDataProviderType() == null ? other.getDataProviderType() == null : this.getDataProviderType().equals(other.getDataProviderType()))
            && (this.getPreDataFields() == null ? other.getPreDataFields() == null : this.getPreDataFields().equals(other.getPreDataFields()))
            && (this.getChooseTables() == null ? other.getChooseTables() == null : this.getChooseTables().equals(other.getChooseTables()))
            && (this.getTableRelations() == null ? other.getTableRelations() == null : this.getTableRelations().equals(other.getTableRelations()))
            && (this.getQueryType() == null ? other.getQueryType() == null : this.getQueryType().equals(other.getQueryType()))
            && (this.getSyntax() == null ? other.getSyntax() == null : this.getSyntax().equals(other.getSyntax()))
            && (this.getBeforeExecute() == null ? other.getBeforeExecute() == null : this.getBeforeExecute().equals(other.getBeforeExecute()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getLastUpdateTime() == null ? other.getLastUpdateTime() == null : this.getLastUpdateTime().equals(other.getLastUpdateTime()))
            && (this.getLastUpdateUserId() == null ? other.getLastUpdateUserId() == null : this.getLastUpdateUserId().equals(other.getLastUpdateUserId()))
            && (this.getCreateUserId() == null ? other.getCreateUserId() == null : this.getCreateUserId().equals(other.getCreateUserId()))
            && (this.getDescription() == null ? other.getDescription() == null : this.getDescription().equals(other.getDescription()))
            && (this.getIsDelete() == null ? other.getIsDelete() == null : this.getIsDelete().equals(other.getIsDelete()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getRepositoryId() == null) ? 0 : getRepositoryId().hashCode());
        result = prime * result + ((getDataProviderType() == null) ? 0 : getDataProviderType().hashCode());
        result = prime * result + ((getPreDataFields() == null) ? 0 : getPreDataFields().hashCode());
        result = prime * result + ((getChooseTables() == null) ? 0 : getChooseTables().hashCode());
        result = prime * result + ((getTableRelations() == null) ? 0 : getTableRelations().hashCode());
        result = prime * result + ((getQueryType() == null) ? 0 : getQueryType().hashCode());
        result = prime * result + ((getSyntax() == null) ? 0 : getSyntax().hashCode());
        result = prime * result + ((getBeforeExecute() == null) ? 0 : getBeforeExecute().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getLastUpdateTime() == null) ? 0 : getLastUpdateTime().hashCode());
        result = prime * result + ((getLastUpdateUserId() == null) ? 0 : getLastUpdateUserId().hashCode());
        result = prime * result + ((getCreateUserId() == null) ? 0 : getCreateUserId().hashCode());
        result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
        result = prime * result + ((getIsDelete() == null) ? 0 : getIsDelete().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", repositoryId=").append(repositoryId);
        sb.append(", dataProviderType=").append(dataProviderType);
        sb.append(", preDataFields=").append(preDataFields);
        sb.append(", chooseTables=").append(chooseTables);
        sb.append(", tableRelations=").append(tableRelations);
        sb.append(", queryType=").append(queryType);
        sb.append(", syntax=").append(syntax);
        sb.append(", beforeExecute=").append(beforeExecute);
        sb.append(", createTime=").append(createTime);
        sb.append(", lastUpdateTime=").append(lastUpdateTime);
        sb.append(", lastUpdateUserId=").append(lastUpdateUserId);
        sb.append(", createUserId=").append(createUserId);
        sb.append(", description=").append(description);
        sb.append(", isDelete=").append(isDelete);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
