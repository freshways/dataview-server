package com.base.vo;

import lombok.Data;

/**
 * 分页参数
 *
 * @author wnhuang
 * @Package com.base.vo
 * @date 2021-03-11 15:25
 */
@Data
public class PageRequestHelperVo {

    private Integer pageSize = 10;

    private Integer pageNum = 1;

    private String keyword;

    private String type;

}
