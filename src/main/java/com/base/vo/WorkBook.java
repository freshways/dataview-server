package com.base.vo;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 工作表
 * @TableName work_book
 */
@TableName(value ="work_book")
@Data
public class WorkBook implements Serializable {
    /**
     *
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId
    private Long id;

    /**
     * 标题
     */
    @TableField("`name`")
    private String name;

    /**
     * 关联数据源Id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long dataSourceId;

    /**
     * 关联数据仓库id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long repositoryId;

    /**
     * 数据仓库类型
     */
    private Integer dataProviderType;

    /**
     * 图表模式
     */
    private Integer chartMode;

    /**
     * 过滤条件
     */
    private String filters;

    /**
     * 行字段
     */
    @TableField("`rows`")
    private String rows;

    /**
     * 列字段
     */
    @TableField("`columns`")
    private String columns;

    /**
     * 颜色配置
     */
    private String colorMark;

    /**
     * 工具提示的文本格式
     */
    private String toolTipsFormat;

    /**
     * 外链参数
     */
    private String urlParams;

    /**
     * 缩放比例
     */
    private Double scale;

    /**
     * 标签配置
     */
    private String labelOption;

    /**
     * 背景色
     */
    private String bgColor;

    /**
     * 数据截取
     */
    @TableField("`limit`")
    private String limit;

    /**
     * 是否开启同环比计算
     */
    private Boolean isYoy;

    /**
     *
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     *  修改时间
     */
    @TableField(fill = FieldFill.UPDATE)
    @OrderBy
    private Date lastUpdateTime;

    /**
     *
     */
    private String lastUpdateUserId;

    /**
     *
     */
    private String createUserId;

    /**
     *
     */
    private String description;

    /**
     * 表格配置
     */
    private String tableTheme;

    /**
     * 指标等级
     */
    private String rankMark;

    /**
     * 自动更新数据源的时间，0表示不更新，单位分钟
     */
    private Double autoUpdate;

    /**
     *
     */
    private String chartConfig;

    /**
     * 逻辑删除
     */
    @TableLogic
    private Byte isDelete;

    /**
     *
     */
    private String produceParameters;

    /**
     *  同环比类型
     */
    private String yoyType;

    /**
     *
     */
    private String dataSnapshot;

    /**
     *
     */
    private String dataMarking;

    /**
     *
     */
    private String havingFilters;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

}
