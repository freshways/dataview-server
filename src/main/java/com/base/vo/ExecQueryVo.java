package com.base.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author wnhuang
 * @Package com.base.vo
 * @date 2021-03-13 19:57
 */
@ApiModel(value = "执行查询对象")
@Data
public class ExecQueryVo implements Serializable {

    @ApiModelProperty(value = "数据库连接主键")
    private Long repositorySourceId;

    @ApiModelProperty(value = "图表主键")
    private Long workBookId;

    @ApiModelProperty(value = "数据源主键")
    private Long dataSourceId;

    @ApiModelProperty(value = "分析主键")
    private Long dashboardId;

    @ApiModelProperty(value = "数据库类型")
    private int dataProviderType;

    @ApiModelProperty(value = "查询的字段集合")
    private List<Field> fields;

    private List<Object> relations;

    @ApiModelProperty(value = "过滤条件集合")
    private List<Filters> filters;

    @ApiModelProperty(value = "sql语句")
    private String sql;

    @ApiModelProperty(value = "分页对象")
    private Map<String,Object> limit;

    @ApiModelProperty(value = "查询类型")
    private Integer queryType;

    @ApiModelProperty(value = "预先执行的SQL语句")
    private String beforeExecute;

    @ApiModelProperty(value = "不使用缓存")
    private Boolean noCache;

    @ApiModelProperty(value = "是否开启同环比")
    private Boolean isYoy = false;

    @ApiModelProperty(value = "同环比类型，按年、月等")
    private String yoyType;
}


