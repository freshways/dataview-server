package com.base.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 查询的每个属性
 *
 * @author wnhuang
 * @Package com.base.vo
 * @date 2021-03-18 23:15
 */
@Data
public class Field implements Serializable {

    //属性名
    private String name;

    //是否分组
    private Boolean group = true;

    //数据源中的名字
    private String systemName;

    //数据类型
    private Integer dataType;

    //维度或度量
    private Integer slaveType;

    //聚合模式
    private Integer aggregateMode;

    //排序方式
    private String sortMode;

    //是否去重
    private Boolean distinct;

    //日期分组格式
    private String dateTimeSub;

    //计算字段表达式
    private String expression;
}
