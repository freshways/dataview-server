package com.base.aspect;

import com.base.utils.Result;
import com.base.utils.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常统一处理类
 * @author wnhuang
 * @Package com.base.advice
 * @date 2021-1-6 16:26
 */
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandle {
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result handle(Exception e) {
        e.printStackTrace();
        return ResultUtil.error(e.getMessage() == null ? "服务器繁忙，请稍后重试。" : e.getMessage());
    }
}
