package com.base.controller;

import com.base.service.WorkBookService;
import com.base.utils.Result;
import com.base.utils.ResultUtil;
import com.base.vo.PageRequestHelperVo;
import com.base.vo.WorkBook;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author wnhuang
 * @Package com.base.controller
 * @date 2021-03-11 15:55
 */
@Api(value = "图表管理API", tags = "Swagger图表管理API")
@RestController
@RequestMapping("/WorkBook")
public class WorkBookController {

//    @Autowired
//    WorkBookService workBookService;
    @Autowired
    WorkBookService workBookService;

    @ApiOperation(value = "分页获取图表列表", notes = "用于前台界面展示、检索图表列表信息")
    @ApiImplicitParam(name = "pageRequestHelperVo", value = "封装的分页请求对象", required = true, dataType = "json",dataTypeClass = PageRequestHelperVo.class,paramType = "query")
    @GetMapping("/getList")
    public Result getList(PageRequestHelperVo pageRequestHelperVo) {
        return ResultUtil.success(workBookService.page(pageRequestHelperVo));
    }

    @GetMapping("/getWorkBook")
    @ApiOperation(value = "获取图表对象")
    public Result getWorkBook(@ApiParam(value = "主键", required = true) Long id) {
        return ResultUtil.success(workBookService.getById(id));
    }

    @ApiOperation(value = "删除图表对象")
    @GetMapping("/delWorkBook")
    public Result delWorkBook(@ApiParam(value = "主键", required = true) Long id) {
        workBookService.removeById(id);
        return ResultUtil.success();
    }

    @ApiOperation(value = "保存图表对象")
    @PostMapping("/saveWorkBook")
    public Result saveWorkBook(@ApiParam(value = "保存的对象", required = true)@RequestBody WorkBook workBook) {
        workBookService.saveOrUpdate(workBook);
        return ResultUtil.success(workBook);
    }
}
