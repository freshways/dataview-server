package com.base.controller;

import com.base.service.ChartInfoService;
import com.base.utils.Result;
import com.base.utils.ResultUtil;
import com.base.vo.ChartInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author wnhuang
 * @Package com.base.controller
 * @date 2021/7/28 21:28
 */
@RestController
@RequestMapping("/chart")
public class ChartController {

    @Autowired
    ChartInfoService chartInfoService;

    @GetMapping("/list")
    public Result list() {
        return ResultUtil.success(chartInfoService.list());
    }

    @PostMapping
    public Result save(@RequestBody ChartInfo chartInfo) {
        chartInfoService.saveOrUpdate(chartInfo);
        return ResultUtil.success(chartInfo);
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable("id") Long id) {
        return ResultUtil.success(chartInfoService.removeById(id));
    }
}
