package com.base.controller;

import com.base.service.DashboardService;
import com.base.utils.Result;
import com.base.utils.ResultUtil;
import com.base.vo.Dashboard;
import com.base.vo.PageRequestHelperVo;
import com.base.vo.RepositorySource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author wnhuang
 * @Package com.base.controller
 * @date 2021/4/14 14:52
 */
@Api(value = "分析展示API", tags = "维护分析界面展示内容")
@RestController
@RequestMapping("/dashboard")
public class DashboardController {

    @Autowired
    DashboardService dashboardService;

    @ApiOperation(value = "分页请求分析列表")
    @GetMapping("/getList")
    public Result getList(PageRequestHelperVo pageRequestHelperVo) {
        return ResultUtil.success(dashboardService.page(pageRequestHelperVo));
    }

    @ApiOperation(value = "新增、修改分析")
    @PostMapping
    public Result save(@RequestBody Dashboard dashboard) {
        dashboardService.saveOrUpdate(dashboard);
        return ResultUtil.success(dashboard);
    }

    @ApiOperation(value = "删除分析")
    @DeleteMapping("/{id}")
    public Result remove(@PathVariable("id") Long id) {
        dashboardService.removeById(id);
        return ResultUtil.success(id);
    }

    @ApiOperation(value = "请求单个分析")
    @GetMapping("/{id}")
    public Result get(@PathVariable("id") Long id) {
        return ResultUtil.success(dashboardService.getById(id));
    }

}
