package com.base.controller;

import com.base.service.LogService;
import com.base.utils.Result;
import com.base.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Collections;

/**
 * 日志查看
 *
 * @author wnhuang
 * @Package com.base.controller
 * @date 2021/8/19 15:30
 */
@RestController
@RequestMapping("/log")
public class LogController {

    @Autowired
    LogService logService;

    private final static String LOG_PATH = "log/";

    @GetMapping()
    public Result logFileNameList() {
        return ResultUtil.success(logService.getLogFileNames(LOG_PATH));
    }

    @PostMapping("/{fileName}")
    public Result logFile(@PathVariable("fileName") String fileName) throws IOException {
        return ResultUtil.success(Collections.singletonList(logService.getLogFileByName(LOG_PATH + fileName)));
    }

}
