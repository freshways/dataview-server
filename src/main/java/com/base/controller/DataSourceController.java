package com.base.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.service.DataSourceService;
import com.base.utils.Result;
import com.base.utils.ResultUtil;
import com.base.vo.DataSource;
import com.base.vo.ExecQueryVo;
import com.base.vo.PageRequestHelperVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author wnhuang
 * @Package com.base.controller
 * @date 2021-03-12 0:57
 */
@Api(value = "数据源管理API", tags = "维护所有的SQL语句脚本")
@RestController
@RequestMapping("/datasource")
public class DataSourceController {

    @Autowired
    private DataSourceService dataSourceService;

    @ApiOperation(value = "分页查询数据源列表", notes = "用于界面展示、检索数据源列表信息")
    @ApiImplicitParam(name = "pageRequestHelperVo", value = "封装的分页请求对象", required = true, dataType = "json", paramType = "query")
    @GetMapping("/getList")
    public Result getList(PageRequestHelperVo pageRequestHelperVo) {
        return ResultUtil.success(dataSourceService.page(pageRequestHelperVo));
    }

    @ApiOperation(value = "查询单个数据源信息")
    @GetMapping("/{id}")
    public Result getDataSource(@PathVariable("id") @ApiParam(value = "主键", required = true) Long id) {
        return ResultUtil.success(dataSourceService.getById(id));
    }

    @ApiOperation(value = "添加、修改数据源对象", notes = "dataSourceId为空时，表示添加，否则修改")
    @ApiImplicitParam(name = "DataSourceVo", value = "保存的对象", required = true, dataType = "json", dataTypeClass = DataSource.class, paramType = "query")
    @PostMapping
    public Result saveDataSource(@RequestBody DataSource dataSource) {
        dataSourceService.saveOrUpdate(dataSource);
        return ResultUtil.success(dataSource);
    }

    @ApiOperation(value = "删除数据源对象")
    @DeleteMapping("/{id}")
    public Result delDataSource(@PathVariable("id") @ApiParam(value = "主键", required = true) Long id) {
        dataSourceService.removeById(id);
        return ResultUtil.success();
    }

    @ApiOperation(value = "获取查询结果")
    @PostMapping("Query3")
    public Result query(@RequestBody @ApiParam(value = "执行查询对象，包含要执行的SQL语句，分页，条件等", required = true) ExecQueryVo execQueryVo) {
        Page page = dataSourceService.execQuery(execQueryVo);
        return ResultUtil.success(page);
    }

}
