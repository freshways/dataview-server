package com.base.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.service.QueryLogService;
import com.base.utils.Result;
import com.base.utils.ResultUtil;
import com.base.vo.QueryLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 数据库查询日志
 *
 * @author wnhuang
 * @Package com.base.controller
 * @date 2021/8/19 15:30
 */
@RestController
@RequestMapping("/queryLog")
public class QueryLogController {

    private QueryLogService queryLogService;

    @Autowired
    public void setQueryLogService(QueryLogService queryLogService) {
        this.queryLogService = queryLogService;
    }

    @PostMapping("/list")
    public Result getQueryLogList(@RequestBody Map<String, Object> requestParam) {
        Page<QueryLog> page = queryLogService.page(requestParam);
        return ResultUtil.success(page);
    }

}
