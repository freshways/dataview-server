package com.base;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@SpringBootTest
class DataviewServerApplicationTests {

    @Test
    void contextLoads() {

    }

    private String url = "jdbc:postgresql://202.168.1.48:5432/cdrdb";
    private String username = "gpadmin";
    private String password = "gpadmin";
    private Connection connection = null;

    @Test
    void testJdbc(){
        try {
            Class.forName("org.postgresql.Driver").newInstance();
            connection = DriverManager.getConnection(url, username, password);
            connection.close();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


    }

}
